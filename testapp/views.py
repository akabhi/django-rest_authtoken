from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from testapp.models import Employee
from testapp.serializers import EmployeeSerializer
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import (
                IsAuthenticated,
                IsAdminUser,
                AllowAny,
                IsAuthenticatedOrReadOnly,
                DjangoModelPermissions,
                DjangoModelPermissionsOrAnonReadOnly,
            )


class EmployeeCRUDCBV(ModelViewSet):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
    authentication_classes = [TokenAuthentication,]
    # permission_classes = [IsAuthenticated,]
    # permission_classes = [IsAdmin,] only admin will be allowed
    # permission_classes = [AllowAny,] anyone can access -default permission
    # permission_classes = [IsAuthenticatedOrReadOnly,] Unauthenticated can perform READ ONLY: GET, HEAD, and OPTIONS
                                                    #   WRITE NOT allowed: POST, PUT, PATCH, DELETE
    permission_classes = [DjangoModelPermissions,] # authentication must be required to access end point
                                                    # GET -> authentication is enough model permission are not required
                                                    # POST, PUT, PATCH, DELETE authentication + model permission | permission could be given through admin interface
                                                    # for superuser -> no model permission required
    # permission_classes = [DjangoModelPermissionsOrAnonReadOnly,] anyone can READ but authentication+permission both required for WRITE

'''
we need to add
    authentication_classes = [TokenAuthentication,]
    permission_classes = [IsAuthenticated,]
to all the classes to activate authentication for all the view

to solve this issue-
1. write a mixin which will be extended by all the views
2. enable authentication globally in settings.py (for all the views)

    REST_FRAMEWORK = {
        'DEFAULT_AUTHENTICATION_CLASSES':('rest_framework.TokenAuthentication',),
        'DEFAULT_PERMISSION_CLASSES':('rest_framework.permissions.IsAuthenticated',)
    }
'''
