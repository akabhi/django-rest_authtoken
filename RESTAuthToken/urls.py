
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from testapp import views

from rest_framework.authtoken import views as vw

router = routers.DefaultRouter()
'''
in case of ModelViewSet base_name is optional
router.register('api', views.EmployeeCRUDCBV, base_name='api')
'''
router.register('api', views.EmployeeCRUDCBV)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path('get-api-token/', vw.obtain_auth_token, name='get-api-token'),
]
