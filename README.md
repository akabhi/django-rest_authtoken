# Django-REST_AuthToken

Authentication on Django REST Framework using Tokens

Import two packages -

    from rest_framework.authentication import TokenAuthentication
    from rest_framework.permissions import (
                    IsAuthenticated,
                    IsAdminUser,
                    AllowAny,
                    IsAuthenticatedOrReadOnly,
                    DjangoModelPermissions,
                    DjangoModelPermissionsOrAnonReadOnly,
                )


authentication for authenticating user to web site.
permission to give access to user for the services.

permission classes -

  1. `IsAuthenticated` : to restrict all the services authenticate
  2. `IsAdminUser` : to provide all the services to admin user only
  3. `AllowAny` : to provide all the services to any user
  4. `IsAuthenticatedOrReadOnly` :
    * unauthenticated user can perform READ operation only e.g. GET, HEAD, OPTIONS
    * only authenticated user can perform WRITE operation e.g. POST, PUT, PATCH, DELETE
    * THESE PERSION CAN BE GIVEN FROM ADMIN INTERFACE
  5. `DjangoModelPermissions` :
    * authentication must be required to access end point
    * authentication is enough, model permission are not required for GET
    * POST, PUT, PATCH, DELETE requires authentication + model permission
    * for superuser, no model permission required
    * THESE PERSION CAN BE GIVEN FROM ADMIN INTERFACE
  6. `DjangoModelPermissionsOrAnonReadOnly:`
    * anyone can READ but authentication + permission both required for WRITE


_**NOTE:-**_

we need to add

    authentication_classes = [TokenAuthentication,]
    permission_classes = [IsAuthenticated,]
to all the classes to activate authentication for all the view

to solve this issue-

1. write a mixin which will be extended by all the views
2. enable authentication globally in settings.py (for all the views)

          REST_FRAMEWORK = {
              'DEFAULT_AUTHENTICATION_CLASSES':('rest_framework.TokenAuthentication',),
              'DEFAULT_PERMISSION_CLASSES':('rest_framework.permissions.IsAuthenticated',)
          }


*to get token -*

1. from Django -

        from rest_framework.authtoken import views
        urlpatterns = [
            path('get-api-token/', views.obtain_auth_token, name='get-api-token'),
        ]

2. from cmd

        http POST http://127.0.0.1:8000/get-api-token/ username="username" password="password"

3. generate from django admin
